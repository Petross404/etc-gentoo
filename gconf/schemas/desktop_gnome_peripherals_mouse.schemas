<?xml version="1.0"?>
<gconfschemafile>
  <schemalist>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/left_handed</key>
      <applyto>/desktop/gnome/peripherals/mouse/left_handed</applyto>
      <owner>gnome</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
        <short>Mouse button orientation</short>
        <long>Swap left and right mouse buttons for left-handed mice.</long>
      </locale>

      <locale name="el">
        <short>Προσανατολισμός πλήκτρου ποντικιού</short>
        <long>Εναλλαγή αριστερού και δεξιού κουμπιού σε ποντίκια για αριστερόχειρες.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/single_click</key>
      <applyto>/desktop/gnome/peripherals/mouse/single_click</applyto>
      <owner>gnome</owner>
      <type>bool</type>
      <default>true</default>
      <locale name="C">
        <short>Single Click</short>
        <long>Single click to open icons.</long>
      </locale>

      <locale name="el">
        <short>Μονό κλικ</short>
        <long>Μονό κλικ για το άνοιγμα εικονιδίων.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/motion_acceleration</key>
      <applyto>/desktop/gnome/peripherals/mouse/motion_acceleration</applyto>
      <owner>gnome</owner>
      <type>float</type>
      <default>-1</default>
      <locale name="C">
        <short>Single Click</short>
        <long>Acceleration multiplier for mouse motion.  A value of -1
        is the system default.</long>
      </locale>

      <locale name="el">
        <short>Μονό κλικ</short>
        <long>Πολλαπλασιαστής συντομεύσεως για τη κίνηση του ποντικιού. Η τιμή -1 είναι η προεπιλεγμένη από το σύστημα.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/motion_threshold</key>
      <applyto>/desktop/gnome/peripherals/mouse/motion_threshold</applyto>
      <owner>gnome</owner>
      <type>int</type>
      <default>-1</default>
      <locale name="C">
        <short>Motion Threshold</short>
        <long>Distance in pixels the pointer must move before
        accelerated mouse motion is activated.  A value of -1 is the
        system default.</long>
      </locale>

      <locale name="el">
        <short>Κατώφλι κίνησης</short>
        <long>Απόσταση σε εικονοστοιχεία την οποία θα πρέπει να διανύσει ο δρομέας πριν να ενεργοποιηθεί η κίνηση συντόμευσης του ποντικιού. Η τιμή -1 είναι η προεπιλεγμένη από το σύστημα.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/drag_threshold</key>
      <applyto>/desktop/gnome/peripherals/mouse/drag_threshold</applyto>
      <owner>gnome</owner>
      <type>int</type>
      <default>8</default>
      <locale name="C">
        <short>Drag Threshold</short>
        <long>Distance before a drag is started.</long>
      </locale>

      <locale name="el">
        <short>Κατώφλι drag</short>
        <long>Απόσταση πριν από την έναρξη συρσίματος.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/double_click</key>
      <applyto>/desktop/gnome/peripherals/mouse/double_click</applyto>
      <owner>gnome</owner>
      <type>int</type>
      <default>400</default>
      <locale name="C">
        <short>Double Click Time</short>
        <long>Length of a double click.</long>
      </locale>

      <locale name="el">
        <short>Χρόνος διπλού κλικ</short>
        <long>Διάρκεια διπλού κλικ.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/locate_pointer</key>
      <applyto>/desktop/gnome/peripherals/mouse/locate_pointer</applyto>
      <owner>gnome</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
        <short>Locate Pointer</short>
        <long>Highlights the current location of the pointer when the
        Control key is pressed and released.</long>
      </locale>

      <locale name="el">
        <short>Εντοπισμός δείκτη</short>
        <long>Επισημαίνει την τρέχουσα θέση του δρομέα όταν έχει πατηθεί και απελευθερωθεί το πλήκτρο Control.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/cursor_font</key>
      <applyto>/desktop/gnome/peripherals/mouse/cursor_font</applyto>
      <owner>gnome</owner>
      <type>string</type>
      <locale name="C">
	<short>Cursor font</short>
	<long>
	  Font name of the cursor. If unset, the default font is
	  used. This value is only propagated to the X server start of
	  each session, so changing it mid-session won't have an effect
	  until the next time you log in.
	</long>
      </locale>

      <locale name="el">
	<short>Γραμματοσειρά δρομέα</short>
	<long>Όνομα γραμματοσειράς του δρομέα. Αν δεν ορισθεί χρησιμοποιείται η προεπιλεγμένη γραμματοσειρά. Αυτή η τιμή ενεργοποιείται κατά την εκκίνηση του εξυπηρετητή X, έτσι η αλλαγή της στη μέση της συνεδρίας δε θα έχει αποτέλεσμα μέχρι την επόμενη είσοδο σας .</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/cursor_theme</key>
      <applyto>/desktop/gnome/peripherals/mouse/cursor_theme</applyto>
      <owner>gnome</owner>
      <type>string</type>
      <locale name="C">
	<short>Cursor theme</short>
	<long>
	  Cursor theme name. Used only by Xservers that support Xcursor,
	  such as XFree86 4.3 and later.
	</long>
      </locale>

      <locale name="el">
	<short>Θέμα δρομέα</short>
	<long>Όνομα θέματος δρομέα. Χρησιμοποιείται μόνο από Xservers που υποστηρίζουν Xcursor, όπως το XFree86 4.3 και νεότερο.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/desktop/gnome/peripherals/mouse/cursor_size</key>
      <applyto>/desktop/gnome/peripherals/mouse/cursor_size</applyto>
      <owner>gnome</owner>
      <type>int</type>
      <default>18</default>
      <locale name="C">
	<short>Cursor size</short>
	<long>
	  Size of the cursor referenced by cursor_theme.
	</long>
      </locale>

      <locale name="el">
	<short>Μέγεθος δρομέα </short>
	<long>Μέγεθος του δρομέα που αναφέρεται από το  cursor_theme.</long>
      </locale>
    </schema>
  </schemalist>
</gconfschemafile>
