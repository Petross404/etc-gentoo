<?xml version="1.0"?>
<gconfschemafile>
  <schemalist>
    <schema>
      <key>/schemas/system/http_proxy/use_http_proxy</key>
      <applyto>/system/http_proxy/use_http_proxy</applyto>
      <owner>gnome-vfs</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
        <short>Use HTTP proxy</short>
        <long>Enables the proxy settings when accessing HTTP over the
        Internet.</long>
      </locale>

      <locale name="el">
        <short>Χρήση διαμεσολαβητή HTTP</short>
        <long>Ενεργοποιεί τις ρυθμίσεις διαμεσολαβητή κατά την πρόσβαση HTTP στο διαδίκτυο.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/host</key>
      <applyto>/system/http_proxy/host</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>HTTP proxy host name</short>
        <long>The machine name to proxy HTTP through.</long>
      </locale>

      <locale name="el">
        <short>Όνομα συστήματος διαμεσολαβητή HTTP</short>
        <long>Το όνομα συστήματος μέσω του οποίου γίνεται διαμεσολάβηση HTTP.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/port</key>
      <applyto>/system/http_proxy/port</applyto>
      <owner>gnome-vfs</owner>
      <type>int</type>
      <default>8080</default>
      <locale name="C">
        <short>HTTP proxy port</short>
        <long>The port on the machine defined by "/system/http_proxy/host"
        that you proxy through.</long>
      </locale>

      <locale name="el">
        <short>Θύρα διαμεσολαβητή HTTP</short>
        <long>Η θύρα στο σύστημα που καθορίζεται από το  &quot;/system/http_proxy/host&quot; μέσω του οποίου γίνεται διαμεσολάβηση.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/use_authentication</key>
      <applyto>/system/http_proxy/use_authentication</applyto>
      <owner>gnome-vfs</owner>
      <type>bool</type>
      <default>false</default>
      <locale name="C">
        <short>Authenticate proxy server connections</short>
        <long>If true, then connections to the proxy server require
        authentication. The username/password combo is defined by
        "/system/http_proxy/authentication_user" and
        "/system/http_proxy/authentication_password".</long>
      </locale>

      <locale name="el">
        <short>Πιστοποίηση συνδέσεων στον εξυπηρετητή διαμεσολάβησης</short>
        <long>Αν αληθές, τότε οι συνδέσεις στο διαμεσολαβητή απαιτούν πιστοποίηση. Τα όνομα χρήστη/κωδικός καθορίζονται από το &quot;/system/http_proxy/authentication_user&quot; και &quot;/system/http_proxy/authentication_password&quot;.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/authentication_user</key>
      <applyto>/system/http_proxy/authentication_user</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>HTTP proxy username</short>
        <long>User name to pass as authentication when doing HTTP proxying.</long>
      </locale>

      <locale name="el">
        <short>Όνομα χρήστη διαμεσολαβητή HTTP</short>
        <long>Όνομα χρήστη για να περάσει ως πιστοποίηση κατά τη διαμεσολάβηση http.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/authentication_password</key>
      <applyto>/system/http_proxy/authentication_password</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>HTTP proxy password</short>
        <long>Password to pass as authentication when doing HTTP proxying.</long>
      </locale>

      <locale name="el">
        <short>Κωδικός διαμεσολαβητή HTTP</short>
        <long>Κωδικός πρόσβασης για πιστοποίηση όταν γίνεται διαμεσολάβηση HTTP.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/http_proxy/ignore_hosts</key>
      <applyto>/system/http_proxy/ignore_hosts</applyto>
      <owner>gnome-vfs</owner>
      <type>list</type>
      <list_type>string</list_type>
      <default>[localhost,127.0.0.0/8]</default>
      <locale name="C">
        <short>Non-proxy hosts</short>
        <long>This key contains a list of hosts which are connected to
	directly, rather than via the proxy (if it is active). The values can
	be hostnames, domains (using an initial wildcard like *.foo.com), IP
	host addresses (both IPv4 and IPv6) and network addresses with a
	netmask (something like 192.168.0.0/24).</long>
      </locale>

      <locale name="el">
        <short>Συστήματα χωρίς διαμεσολαβητή</short>
        <long>Αυτό το κλειδί περιέχει μια λίστα από συστήματα στα οποία γίνεται απευθείας σύνδεση, παρά μέσω του διαμεσολαβητή (αν είναι ενεργός). Οι τιμές μπορεί να είναι ονόματα συστήματος, τομείς (με τη χρήση μιας αρχικής wildcard όπως *.foo.com), διευθύνσεις IP (και IPv4 και IPv6) και διευθύνσεις δικτύου με  netmask (κάτι σαν 192.168.0.0/24).</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/mode</key>
      <applyto>/system/proxy/mode</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default>none</default>
      <locale name="C">
        <short>Proxy configuration mode</short>
        <long>Select the proxy configuration mode. Supported values are
	"none", "manual", "auto".</long>
      </locale>

      <locale name="el">
        <short>Λειτουργία ρυθμίσεων διαμεσολαβητή</short>
        <long>Επιλογή λειτουργίας ρύθμισης διαμεσολαβητή. Υποστηριζόμενες τιμές είναι &quot;καμμία&quot;, &quot;χειροκίνητη&quot;, &quot;αυτόματη&quot;.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/secure_host</key>
      <applyto>/system/proxy/secure_host</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>Secure HTTP proxy host name</short>
        <long>The machine name to proxy secure HTTP through.</long>
      </locale>

      <locale name="el">
        <short>Όνομα συστήματος ασφαλούς διαμεσολαβητή HTTP</short>
        <long>Το όνομα συστήματος μέσω του οποίου γίνεται διαμεσολάβηση ασφαλούς HTTP.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/secure_port</key>
      <applyto>/system/proxy/secure_port</applyto>
      <owner>gnome-vfs</owner>
      <type>int</type>
      <default>0</default>
      <locale name="C">
        <short>Secure HTTP proxy port</short>
        <long>The port on the machine defined by "/system/proxy/secure_host"
        that you proxy through.</long>
      </locale>

      <locale name="el">
        <short>Θύρα ασφαλούς διαμεσολαβητή HTTP</short>
        <long>Η θύρα στο σύστημα που καθορίζεται από το &quot;/system/proxy/secure_host&quot; μέσω του οποίου γίνεται διαμεσολάβηση.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/ftp_host</key>
      <applyto>/system/proxy/ftp_host</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>FTP proxy host name</short>
        <long>The machine name to proxy FTP through.</long>
      </locale>

      <locale name="el">
        <short>Όνομα συστήματος διαμεσολαβητή FTP </short>
        <long>Το όνομα συστήματος μέσω του οποίου γίνεται διαμεσολάβηση FTP.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/ftp_port</key>
      <applyto>/system/proxy/ftp_port</applyto>
      <owner>gnome-vfs</owner>
      <type>int</type>
      <default>0</default>
      <locale name="C">
        <short>FTP proxy port</short>
        <long>The port on the machine defined by "/system/proxy/ftp_host"
        that you proxy through.</long>
      </locale>

      <locale name="el">
        <short>Θύρα διαμεσολαβητή FTP </short>
        <long>Η θύρα στο σύστημα που καθορίζεται από το &quot;/system/proxy/ftp_host&quot; μέσω του οποίου γίνεται διαμεσολάβηση.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/socks_host</key>
      <applyto>/system/proxy/socks_host</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>SOCKS proxy host name</short>
        <long>The machine name to proxy socks through.</long>
      </locale>

      <locale name="el">
        <short>Όνομα συστήματος διαμεσολαβητή SOCKS</short>
        <long>Το όνομα συστήματος μέσω του οποίου γίνεται διαμεσολάβηση socks.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/socks_port</key>
      <applyto>/system/proxy/socks_port</applyto>
      <owner>gnome-vfs</owner>
      <type>int</type>
      <default>0</default>
      <locale name="C">
        <short>SOCKS proxy port</short>
        <long>The port on the machine defined by "/system/proxy/socks_host"
        that you proxy through.</long>
      </locale>

      <locale name="el">
        <short>Θύρα διαμεσολαβητή SOCKS</short>
        <long>Η θύρα στο σύστημα που καθορίζεται από το  &quot;/system/proxy/socks_host&quot; μέσω του οποίου γίνεται διαμεσολάβηση.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/system/proxy/autoconfig_url</key>
      <applyto>/system/proxy/autoconfig_url</applyto>
      <owner>gnome-vfs</owner>
      <type>string</type>
      <default></default>
      <locale name="C">
        <short>Automatic proxy configuration URL</short>
        <long>URL that provides proxy configuration values.</long>
      </locale>

      <locale name="el">
        <short>Αυτόματη ρύθμιση URL ρύθμισης διαμεσολαβητή</short>
        <long>Το URL που παρέχει τιμές ρύθμισης διαμεσολαβητή.</long>
      </locale>
    </schema>
  </schemalist>
</gconfschemafile>
