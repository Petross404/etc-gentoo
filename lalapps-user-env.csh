# source this file to access 'lalapps'
if ( ! ${?LAL_DATA_PATH} ) setenv LAL_DATA_PATH
setenv LAL_DATA_PATH `echo ":${LAL_DATA_PATH}:" | /bin/sed 's|:/usr/share/lalapps:|:|;s|::*|:|g;s|^:||;s|:$||'`
setenv LAL_DATA_PATH "/usr/share/lalapps:${LAL_DATA_PATH}"
if ( ! ${?PYTHONPATH} ) setenv PYTHONPATH
setenv PYTHONPATH `echo ":${PYTHONPATH}:" | /bin/sed 's|:/usr/lib64/python3.4/site-packages:|:|;s|::*|:|g;s|^:||;s|:$||'`
setenv PYTHONPATH "/usr/lib64/python3.4/site-packages:${PYTHONPATH}"
if ( ! ${?PATH} ) setenv PATH
setenv PATH `echo ":${PATH}:" | /bin/sed 's|:/usr/bin:|:|;s|::*|:|g;s|^:||;s|:$||'`
setenv PATH "/usr/bin:${PATH}"
if ( ! ${?MANPATH} ) setenv MANPATH
setenv MANPATH `echo ":${MANPATH}:" | /bin/sed 's|:/usr/share/man:|:|;s|::*|:|g;s|^:||;s|:$||'`
setenv MANPATH "/usr/share/man:${MANPATH}"
