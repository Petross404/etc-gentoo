# source this file to access 'lalapps'
LAL_DATA_PATH=`echo ":${LAL_DATA_PATH}:" | /bin/sed 's|:/usr/share/lalapps:|:|;s|::*|:|g;s|^:||;s|:$||'`
LAL_DATA_PATH="/usr/share/lalapps:${LAL_DATA_PATH}"
export LAL_DATA_PATH
PYTHONPATH=`echo ":${PYTHONPATH}:" | /bin/sed 's|:/usr/lib64/python3.4/site-packages:|:|;s|::*|:|g;s|^:||;s|:$||'`
PYTHONPATH="/usr/lib64/python3.4/site-packages:${PYTHONPATH}"
export PYTHONPATH
PATH=`echo ":${PATH}:" | /bin/sed 's|:/usr/bin:|:|;s|::*|:|g;s|^:||;s|:$||'`
PATH="/usr/bin:${PATH}"
export PATH
MANPATH=`echo ":${MANPATH}:" | /bin/sed 's|:/usr/share/man:|:|;s|::*|:|g;s|^:||;s|:$||'`
MANPATH="/usr/share/man:${MANPATH}"
export MANPATH
