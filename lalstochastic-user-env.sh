# source this file to access 'lalstochastic'
LALSTOCHASTIC_PREFIX="/usr"
export LALSTOCHASTIC_PREFIX
LAL_DATA_PATH=`echo ":${LAL_DATA_PATH}:" | /bin/sed 's|:/usr/share/lalstochastic:|:|;s|::*|:|g;s|^:||;s|:$||'`
LAL_DATA_PATH="/usr/share/lalstochastic:${LAL_DATA_PATH}"
export LAL_DATA_PATH
PKG_CONFIG_PATH=`echo ":${PKG_CONFIG_PATH}:" | /bin/sed 's|:/usr/lib64/pkgconfig:|:|;s|::*|:|g;s|^:||;s|:$||'`
PKG_CONFIG_PATH="/usr/lib64/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH
LD_LIBRARY_PATH=`echo ":${LD_LIBRARY_PATH}:" | /bin/sed 's|:/usr/lib64:|:|;s|::*|:|g;s|^:||;s|:$||'`
LD_LIBRARY_PATH="/usr/lib64:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH
PATH=`echo ":${PATH}:" | /bin/sed 's|:/usr/bin:|:|;s|::*|:|g;s|^:||;s|:$||'`
PATH="/usr/bin:${PATH}"
export PATH
MANPATH=`echo ":${MANPATH}:" | /bin/sed 's|:/usr/share/man:|:|;s|::*|:|g;s|^:||;s|:$||'`
MANPATH="/usr/share/man:${MANPATH}"
export MANPATH
