# This is an example configuration file.

# Client example.  As you can see you need to specify two address
# ranges, one for the source network and one for the destination
# network, and a single IP address of the remote VPN server (in
# our example redacted using XXX.XXX.XXX.XXX).  As many network
# ranges and remote servers as are required may be specified.
# Client files should be named after the interface which will
# be used to access the VPN (Eg. ppp0).

# Source        Destination     Via

10.0.0.0/16     10.1.0.0/16     XXX.XXX.XXX.XXX


# Server example.  As you can see you only need to specify two
# address ranges, one for the source network and one for the 
# destination network.  The address of the remote VPN clients
# will be determined automatically.  As many network ranges 
# as are required may be specified.  Server files should be
# named after the Phase-1 ID used by the corresponding client.

# Source        Destination

10.1.0.0/16     10.0.0.0/16
